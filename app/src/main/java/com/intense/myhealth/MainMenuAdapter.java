package com.intense.myhealth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Intense on 28-9-2015.
 */
public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.ViewHolder> {

    private List<MainMenuItem> mMainMenuItems;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Context mContext;

        private TextView mTitle;
        private TextView mSubtitle;
        private ImageView mIcon;

        public ViewHolder(Context context, View itemView) {
            super(itemView);

            mContext = context;

            mTitle = (TextView) itemView.findViewById(R.id.bill_price);
            mSubtitle = (TextView) itemView.findViewById(R.id.bill_date);
            mIcon = (ImageView) itemView.findViewById(R.id.main_menu_item_icon);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            String title = ((TextView) v.findViewById(R.id.bill_price)).getText().toString();

            if (title.equals(mContext.getString(R.string.menu_take_photo))) {
                mContext.startActivity(new Intent("android.media.action.IMAGE_CAPTURE"));
                return;
            }
            if (title.equals(mContext.getString(R.string.menu_measurements))) {
                mContext.startActivity(new Intent(mContext, MeasurementsActivity.class));
                return;
            }
            if (title.equals(mContext.getString(R.string.menu_bills))) {
                mContext.startActivity(new Intent(mContext, BillsActivity.class));
                return;
            }
            if (title.equals(mContext.getString(R.string.menu_bluetooth_connect))) {
                mContext.startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
                return;
            }
            if (title.equals(mContext.getString(R.string.menu_logout))) {
                ((Activity) mContext).finish();
                //return;
            }
        }
    }

    public MainMenuAdapter(List<MainMenuItem> menuItems) {

        mMainMenuItems = menuItems;
    }

    @Override
    public MainMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.main_menu_item, viewGroup, false);

        return new ViewHolder(context, contactView);
    }

    @Override
    public void onBindViewHolder(MainMenuAdapter.ViewHolder viewHolder, int i) {

        MainMenuItem mainMenuItem = mMainMenuItems.get(i);

        TextView itemTitle = viewHolder.mTitle;
        TextView itemSubtitle = viewHolder.mSubtitle;
        //ImageView itemIcon = viewHolder.mIcon;

        itemTitle.setText(mainMenuItem.getTitle());
        itemSubtitle.setText(mainMenuItem.getSubtitle());
        //itemIcon.setImageResource(mainMenuItem.getIconResId());

    }

    @Override
    public int getItemCount() {
        return mMainMenuItems.size();
    }
}
