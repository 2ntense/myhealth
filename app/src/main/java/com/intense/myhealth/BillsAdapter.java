package com.intense.myhealth;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Intense on 28-9-2015.
 */
public class BillsAdapter extends RecyclerView.Adapter<BillsAdapter.ViewHolder> {

    private List<Bill> mBills;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Context mContext;

        private TextView mTitle;
        private TextView mSubtitle;
        //private ImageView mIcon;

        public ViewHolder(Context context, View itemView) {
            super(itemView);

            mContext = context;

            mTitle = (TextView) itemView.findViewById(R.id.bill_price);
            mSubtitle = (TextView) itemView.findViewById(R.id.bill_date);
            //mIcon = (ImageView) itemView.findViewById(R.id.main_menu_item_icon);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

        }
    }

    public BillsAdapter(List<Bill> bill) {

        mBills = bill;
    }

    @Override
    public BillsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.bill, viewGroup, false);

        return new ViewHolder(context, contactView);
    }

    @Override
    public void onBindViewHolder(BillsAdapter.ViewHolder viewHolder, int i) {

        Bill bill = mBills.get(i);

        TextView itemTitle = viewHolder.mTitle;
        TextView itemSubtitle = viewHolder.mSubtitle;
        //ImageView itemIcon = viewHolder.mIcon;

        itemTitle.setText(String.valueOf("€ " + bill.getPrice()));
        itemSubtitle.setText(bill.getDate());
        //itemIcon.setImageResource(mainMenuItem.getIconResId());

    }

    @Override
    public int getItemCount() {
        return mBills.size();
    }
}
