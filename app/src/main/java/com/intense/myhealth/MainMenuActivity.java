package com.intense.myhealth;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainMenuActivity extends Activity {

    private RecyclerView mMainMenuList;
    private RecyclerView.Adapter mMainMenuAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private JSONObject accountDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        setTitle("MyHealth - Main Menu");
        initMenu();


        try {

            String jsonString = getIntent().getExtras().getString("jsonResponse");
            String jsonStringF = jsonString.substring(1, jsonString.length() - 1);
            String jsonStringFF = jsonStringF.replaceAll("\\\\", "");

            accountDetails = new JSONObject(jsonStringFF);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.account_details) {
            showAccountDetailsDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initMenu() {

        List<MainMenuItem> items = new ArrayList<>();

        items.add(new MainMenuItem(this, R.string.menu_take_photo, "Take photo of urine test", android.R.drawable.ic_menu_camera));
        items.add(new MainMenuItem(this, R.string.menu_measurements, "List of measurements", 0));
        items.add(new MainMenuItem(this, R.string.menu_bills, "List of bills", android.R.drawable.ic_menu_info_details));
        items.add(new MainMenuItem(this, R.string.menu_bluetooth_connect, "Connect to Bluetooth measurement device", android.R.drawable.stat_sys_data_bluetooth));
        items.add(new MainMenuItem(this, R.string.menu_logout, "End session", android.R.drawable.ic_menu_close_clear_cancel));


        mMainMenuList = (RecyclerView) findViewById(R.id.main_menu_list);
        mMainMenuAdapter = new MainMenuAdapter(items);
        mLayoutManager = new LinearLayoutManager(this);

        mMainMenuList.setLayoutManager(mLayoutManager);
        mMainMenuList.setAdapter(mMainMenuAdapter);
    }

    private void showAccountDetailsDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_account_details);

        TextView tvAccountDetails = (TextView) dialog.findViewById(R.id.account_details);

        try {
            tvAccountDetails.setText("First name: " + accountDetails.getString("first_name") + "\n"
                            + "last_name" + accountDetails.getString("last_name") + "\n"
                            + "Email: " + accountDetails.getString("email")
            );


        } catch (JSONException e) {
            e.printStackTrace();
        }

        dialog.show();
    }

}
