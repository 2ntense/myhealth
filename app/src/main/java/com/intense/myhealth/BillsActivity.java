package com.intense.myhealth;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class BillsActivity extends Activity {

    private final String sampleJson = "[{date: \"31-01-2015\", price: \"6.70\"},{date: \"28-02-2015\", price: \"7.10\"},{date: \"31-03-2015\",price: \"6.30\"},{date: \"30-04-2015\", price: \"5.80\"},{date: \"31-05-2015\", price: \"7.30\"},{date: \"30-06-2015\", price: \"9.60\"},{date:  \"31-07-2015\", price: \"4.00\"},{date:  \"31-08-2015\", price: \"5.00\"},{date:  \"30-09-2015\", price: \"6.20\"}]";

    private RecyclerView mBillList;
    private BillsAdapter mBillsAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bills);
        setTitle("Bills");


        try {
            List<Bill> bills = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(sampleJson);

            for (int i = 0; i < jsonArray.length(); i++) {

                String date = jsonArray.getJSONObject(i).getString("date");
                double price = jsonArray.getJSONObject(i).getDouble("price");

                Bill bill = new Bill(date, price);
                bills.add(bill);

            }

            mBillList = (RecyclerView) findViewById(R.id.bill_list);
            mBillsAdapter= new BillsAdapter(bills);
            mLayoutManager = new LinearLayoutManager(this);

            mBillList.setLayoutManager(mLayoutManager);
            mBillList.setAdapter(mBillsAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bills, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
