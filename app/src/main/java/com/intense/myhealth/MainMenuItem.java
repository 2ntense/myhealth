package com.intense.myhealth;

import android.content.Context;

/**
 * Created by Intense on 29-9-2015.
 */
public class MainMenuItem {

    private Context mContext;

    private int mTitleResId;
    private String mSubtitle;
    private int mIconResId;

    public MainMenuItem(Context context, int titleResId, String subtitle, int iconResId) {

        mContext = context;

        mTitleResId = titleResId;
        mSubtitle = subtitle;
        mIconResId = iconResId;
    }

    public String getTitle() {
        return mContext.getString(mTitleResId);
    }

    public int getTitleId() {
        return mTitleResId;
    }

    public void setTitle(int titleResId) {
        mTitleResId = titleResId;
    }

    public String getSubtitle() {
        return mSubtitle;
    }

    public void setSubtitle(String mSubtitle) {
        this.mSubtitle = mSubtitle;
    }

    public int getIconResId() {
        return mIconResId;
    }

    public void setIconResId(int mIconResId) {
        this.mIconResId = mIconResId;
    }
}
