package com.intense.myhealth;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends Activity {

    private static final String LOGIN_API_URL = "http://192.168.0.104/myh/MyHealth-Website/public/api/login";

    private EditText mEtUsername;
    private EditText mEtPassword;
    private Button mBLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEtUsername = (EditText) findViewById(R.id.login_username);
        mEtPassword = (EditText) findViewById(R.id.login_password);
        mBLogin = (Button) findViewById(R.id.login_submit_button);

        initSignIn();

    }

    private void initSignIn() {

        //DEBUG
        mEtUsername.setText("gebruiker");
        mEtPassword.setText("asdasd");

        final String[] loginParameters = new String[2];
        loginParameters[0] = mEtUsername.getText().toString();
        loginParameters[1] = mEtPassword.getText().toString();

        mBLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new login().execute(loginParameters);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Login asynctask class
     */
    private class login extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(final String... loginParameters) {

            RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);

            StringRequest sr = new StringRequest(Request.Method.POST, LOGIN_API_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    System.out.println(response);

                    Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                    intent.putExtra("jsonResponse", response);
                    startActivity(intent);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("LOGIN", error.toString());
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("username", loginParameters[0]);
                    params.put("password", loginParameters[1]);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };

            queue.add(sr);

            return null;
        }
    }
}
