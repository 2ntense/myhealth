package com.intense.myhealth;

/**
 * Created by Intense on 1-10-2015.
 */
public class Bill {

    private String date;
    private double price;

    public Bill(String date, double price) {
        this.date = date;
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
